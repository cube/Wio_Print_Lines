#include "TFT_eSPI.h"

TFT_eSPI tft;

void setup() {
  tft.begin(); // Set up screen
  tft.setRotation(3); // This can be any value 0-3 to your preference
  tft.fillScreen(TFT_BLACK); // Clear the screen  to black
  tft.setFreeFont(&FreeSans9pt7b); // Set the font
  tft.setTextColor(TFT_GREENYELLOW); // Title colour
  tft.drawString("cube @ codeberg", 10, 10); // I usually use a title reserved for the first line starting at 10.
  // You can delete the title to make space for an extra line, but be sure to adjust the pos below!

  // Set the text colour to white, then loop through the lines to print them
  tft.setTextColor(TFT_WHITE);
  int pos = 30; // This is where the first line after the title starts

  int number_of_lines = 10; // Tweak this value to however many lines you want to generate. 
                            // There is no error if they don't fit on the screen.

  for(int x = 0; x < number_of_lines; x++) 
  {
    String text = "Line " + (String)(x+1); // Creates the string "Line 1", "Line 2", etc.
    tft.drawString(text, 10, pos); // Print the string

    pos += 20; // Tweak this according to how spaced out you want each line to be. Less = closer, More = further apart.
    // 20 allows for 10 lines of text (plus the title), 25 allows for 8 lines, 30 allows for 7 lines. Experiment with it.
  }
}

void loop() {}
